# --------------------- String
Palabra = 'Palabra'
Palabras = "Esto es un texto"

# --------------------- Enteros - int
Entero = 210

# --------------------- Reales - Float
Reales = 12.5

#  --------------------- Complejos
Complejos = 1j
print(Palabra, Palabras, Entero, Reales, Complejos)

# --------------------- Listas
Lista = [1, 2, 3, 4, 5, 6, "Miguel"]
print(Lista)

#De este modo se puede imprimir un unico valor por medio de su indice
print(Lista[0])
print(Lista[1])

#Metodo de una lista
Lista.append(7) # solo permite agrar un solo valor
print(Lista)

Lista2 = Lista.copy() # Permite copiar el valor de una lista a otra
print(Lista)

Lista.append(8)
print(Lista)

Lista2.append(3)
print(Lista2.count(3)) #Permite saver cuantas veces se repite un valor en la lista

print(len(Lista)) #Permite saver la longitud en la lista

Lista.pop() #Permite eliminar el ultimo elemento de la lista
print(Lista)

Lista.remove(5) #Permite eliminar un elemento en especifico, por su valor
print(Lista)

Lista.reverse() #Permite ordenar la lista al reverso de como se encuentra
print(Lista)

Lista.clear() # Permite limpiar la lista
print(Lista)

Lista2.remove("Miguel")
#Permite ordenar una lista, PERO deven de ser del mismo tipo de dato
Lista2.sort() 
print(Lista2)

Lista2.insert(2,"Jose")#Permite insertar un valor en la lista, por el indice
print(Lista2)

Tupla = tuple(Lista) #Conviertiendo un lista a una tupla de datos.

# --------------------- Tuplas
tupla = ("Hola", "Miguel", "Parra", "Muñoz")

print(tupla[3]) #De este modo puedo imprimir un valor especifico por su indice 

print(tupla[-2])# De este modo se puede imprimir el resultado de manera inversa.

print(tupla[0:2])# De este modo se excluye el indice 2 de la tupla

print (len(tupla)) #Perite saber la logitud de la tupla

#Metodos
print(tupla.count(3)) #Permite saver cuantas veces se repite un valor en una tupla.

print (tupla.index("Parra")) # Permite saber el indice en donde se encuentra un valor
 
#Para modificar los elementos de una tupla, es necesario transformarla a una lista

ListaDeTupla = list(tupla) # <-- De este modo se puede transformar una tupla en lista

ListaDeTupla.append("Jose") #Agregando un nuevo valor a la lista
print (ListaDeTupla)

# Cabe recordar que una tupla no es modificable, ni se agregar ni se elimina elementos.
del tupla # De este modo se elimina una tupla


# --------------------- Ranges
rango = range(6) # De este modo puedo declarar un rango definido de 0 a 6
print (rango)

# --------------------- Diccionarios
diccionario ={
    "Nombre":"Miguel",
    "Apellido":"Parra",
    "Edad":21,
    "Sexo":"Masculino"
}
print(diccionario)
print(diccionario["Nombre"])

print(len(diccionario)) #De este modo se permite saber la longitud del diccionario

#Tambien se puede declarar diccionarios con el constructor "dict"
diccionario2 = dict(nombre = "Mathyas", apellido = "Parra", edad = 8 )
print(diccionario2)

print("Nombre" in diccionario)#Permite saber si el valor de esa llave esta dentro del diccionario

#Metodos 
print(diccionario.get("Apellido")) #De este modo se puede acceder a un valor de un diccionario

diccionario["Estudios"] = "Ing. en sistemas" #De este modo puedo agregar y cambiar el valor a un elemento dentro de un diccionario
print(diccionario)
  
diccionario.popitem() #Permite eliminar el ultimo valor en especifico.
print(diccionario)

diccionario.pop("Sexo") #Permite eliminar un valor en especifico por su clave
print(diccionario)

del diccionario["Apellido"]# De este modo se elmina el diccionario
print(diccionario)

copiaDiccionario = diccionario.copy()#De este modo se puede copiar el contenido de un diccionario
print(diccionario, copiaDiccionario)

copiaDiccionario = dict(diccionario) # De este modo tambien se puede realizar una copia
print(copiaDiccionario)

copiaDiccionario.clear() #De este modo se puede limpiar el contenido de un diccionario
print(copiaDiccionario)

#Anidadcion de diccionarios
#Esta es una de las formas de anidar diccionarios
Gatos = {
    "Fluffy":{
        "nombre":"Fluffy",
        "raza":"Persa"
    },
    "Copito":{
        "nombre":"Copito",
        "raza":"Chola"
    }
}

print (Gatos)

#Esta es otra forma de adinar diccionarios
fluffy = {
    "nombre":"Fluffy",
    "raza":"Persa"
}
copito = {
    "nombre":"Copito",
    "raza":"Chola"
}

Gatos = {
    "Fluffy": fluffy,
    "Copito": copito
}
print (Gatos)

# --------------------- Booleans
Bool1 = True
Bool2 = False

print (Bool1, Bool2)
