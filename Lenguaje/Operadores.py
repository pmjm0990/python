# Operadores del lenguaje


# Operadores aritmeticos
print("Suma -> +")
print(3 + 2)

print("Resta -> -")
print(5 - 2)

print("Multi -> *")
print(3 * 5)

print("Div -> /")
print(5 / 4)
print(5.0 / 4.0)

print("Div de enteros -> /")
print(5 // 4)

print("Mod -> %")
print(9 % 2)
print(10 % 2)

print("Potencia -> **")
print(3 ** 2)

# Operadores relacionales
print("Mayor que -> 3 > 5")
print(3 > 5)

print("Mayor igual que -> 10 >= 11")
print(10 >= 11)

print("Menor que -> 5 > 10")
print(5 < 10)

print("Menor igual que -> 10 <= 11")
print(10 <= 11)

print("Igual que -> 10 == 10")
print(10 == 10)

print("Diferente de -> 10 != 11")
print(10 != 11)

# Operadores logicos 

print(3 == 3 and 4 == 4)
print(3 < 5 or 6 > 2)

print()
